import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {D3Service} from 'd3-ng2-service';
import { LageplanComponent } from './lageplan/lageplan.component';
import { Dyn3Component } from './dyn3/dyn3.component';

@NgModule({
  declarations: [
    AppComponent,
    LageplanComponent,
    Dyn3Component,
  ],
  imports: [
    BrowserModule
  ],
  providers: [D3Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
