import {Construct} from './lageplan.construct';

export class Building extends Construct {

  private static instanceCount = 0;
  public static readonly codeNichtVersichert = 998;

  constructor(code = Building.codeNichtVersichert) {
    super(String(Building.instanceCount++), code);
  }
}
