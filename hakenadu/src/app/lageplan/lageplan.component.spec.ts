import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LageplanComponent } from './lageplan.component';

describe('LageplanComponent', () => {
  let component: LageplanComponent;
  let fixture: ComponentFixture<LageplanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LageplanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LageplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
