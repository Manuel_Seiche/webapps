export class Construct {

  public pos: string;
  public code: number;
  public x: number;
  public y: number;
  public width: number;
  public height: number;
  public rotate: number;
  public fill: string;
  public stroke: string;

  constructor(pos: string, code: number) {
    this.pos = pos;
    this.code = code;
    this.rotate = 0;
    this.fill = '#BBBBBB';
    this.stroke = '#000000';
  }

  centerX(): number {
    return this.x + this.width / 2;
  }

  centerY(): number {
    return this.y + this.height / 2;
  }
}
