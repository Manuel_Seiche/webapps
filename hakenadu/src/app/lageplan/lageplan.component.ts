///<reference path='../../../node_modules/@angular/core/src/metadata/directives.d.ts'/>
import {Component, OnInit} from '@angular/core';
import {Construct} from './lageplan.construct';
import {D3, D3Service, Selection} from 'd3-ng2-service';
import {Building} from './lageplan.building';
import {Arrow} from './lageplan.arrow';

@Component({
  selector: 'app-lageplan',
  templateUrl: './lageplan.component.html',
  styleUrls: ['./lageplan.component.css']
})
export class LageplanComponent implements OnInit {

  private static selectionStroke = '#EEA64D';
  private static minSideLength = 10;
  private static defaultSideLength = 50;

  private d3: D3;
  private svg: Selection<any, any, any, any>;
  private buildings: Array<Building>;
  private arrows: Array<Arrow>;

  private width: number;
  private height: number;

  private selectedConstruct: Construct;
  private pressed: boolean;
  private moveX: number;
  private moveY: number;

  constructor(d3Service: D3Service) {
    this.d3 = d3Service.getD3();
    this.width = window.innerWidth * 0.75;
    this.height = window.innerHeight * 0.75;
    this.initializeConstructs();
  }

  initializeConstructs(): void {
    // Test-Construct 1
    const b1 = new Building();
    b1.x = 50;
    b1.y = 50;
    b1.width = 100;
    b1.height = 50;
    b1.fill = '#FF0000';
    // Test-Construct 2
    const b2 = new Building();
    b2.x = 150;
    b2.y = 150;
    b2.width = 50;
    b2.height = 100;
    b2.fill = '#0000FF';
    // Add Buildings to Dataset
    this.buildings = [b1, b2];
    // Test-Arrow
    const a1 = new Arrow();
    a1.x = 20;
    a1.y = 20;
    a1.width = 200;
    a1.height = 200;
    a1.fill = '#00FF00';
    // Add Arrows to Dataset
    this.arrows = [a1];
  }

  ngOnInit() {
    this.svg = this.d3.select('body').append('svg')
      .attr('width', this.width)
      .attr('height', this.height)
      .style('border', '2px solid black')
      .style('background', '#FEFE220F')
      .on('mousedown', this.handleMouseDown.bind(this))
      .on('mouseup', this.handleMouseUp.bind(this))
      .on('mousemove', this.handleMouseMove.bind(this))
      .on('contextmenu', this.handleContextMenu.bind(this));

    const g = this.svg.selectAll('g')
      .data(this.buildings)
      .enter().append('g');

    this.updateStaticFields(
      this.updateTransform(
        this.updateBuildingDynamicFields(
          g.append('rect'))));

    const defs = this.svg.append('defs');
    this.appendMarkerToDef('arrow', defs, '#000000');
    this.appendMarkerToDef('arrow-selected', defs, LageplanComponent.selectionStroke);

    this.updateArrowStaticFields(
      this.updateTransform(
        this.updateArrowDynamicFields(
          this.svg.selectAll('line')
            .data(this.arrows)
            .enter().append('line')
        )));
    // g.append('text')
    //   .text(building => building.pos)
    //   .attr('x', building => building.centerX())
    //   .attr('y', building => building.centerY())
    //   .attr('dy', '0.35em');
  }

  appendMarkerToDef(id: string, defSelection: Selection<any, any, any, any>, fill: string): void {
    defSelection.append('marker')
      .attr('id', id)
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 5)
      .attr('refY', 0)
      .attr('markerWidth', 8)
      .attr('markerHeight', 8)
      .attr('orient', 'auto')
      .attr('fill', fill)
      .append('path')
      .attr('d', 'M0,-5L10,0L0,5')
      .attr('class', 'arrowHead');
  }

  handleMouseDown(): void {
    this.pressed = true;
    this.selectConstruct(null);
  }

  handleMouseUp(): void {
    this.pressed = false;
  }

  handleContextMenu(): void {
    this.d3.event.preventDefault();
  }

  handleMouseMove(): void {
    if (this.selectedConstruct != null && this.pressed) {
      const coords = this.d3.mouse(this.svg.node());
      const mouseButton = this.d3.event.which;
      if (mouseButton === 2) { // scroll wheel
        this.selectedConstruct.rotate = Math.atan2(
          coords[1] - this.selectedConstruct.centerY(),
          coords[0] - this.selectedConstruct.centerX());
      } else {
        const additionX = coords[0] - this.moveX;
        const additionY = coords[1] - this.moveY;

        if (mouseButton === 1) {// left button
          this.selectedConstruct.x += additionX;
          this.selectedConstruct.y += additionY;
        } else if (mouseButton === 3) {
          this.selectedConstruct.width = Math.max(LageplanComponent.minSideLength, this.selectedConstruct.width + additionX);
          this.selectedConstruct.height = Math.max(LageplanComponent.minSideLength, this.selectedConstruct.height + additionY);
        }

        this.moveX = coords[0];
        this.moveY = coords[1];
      }

      this.updateTransform(
        this.updateBuildingDynamicFields(
          this.svg.selectAll('g').selectAll('rect')));

      this.updateTransform(
        this.updateArrowDynamicFields(
          this.svg.selectAll('line')));
    }
  }

  updateStaticFields<S extends Selection<any, any, any, any>>(selection: S): S {
    return selection
      .style('stroke-width', '5px')
      .on('mousedown', this.selectConstruct.bind(this))
      .on('mouseup', this.handleMouseUpOnConstruct.bind(this));
  }

  updateArrowStaticFields<S extends Selection<any, any, any, any>>(selection: S): S {
    return this.updateStaticFields(selection
      .attr('id', 'triangle')
      .attr('stroke', arrow => arrow.stroke));
  }

  updateDynamicFields<S extends Selection<any, any, any, any>>(selection: S): S {
    return selection
      .attr('transform-origin', building => building.centerX() + ' ' + building.centerY())
      .style('stroke', this.requestConstructStroke.bind(this));
  }

  updateArrowDynamicFields<S extends Selection<any, any, any, any>>(selection: S): S {
    return this.updateDynamicFields(selection
      .attr('x1', arrow => arrow.x)
      .attr('y1', arrow => arrow.y)
      .attr('x2', arrow => arrow.x + arrow.width)
      .attr('y2', arrow => arrow.y + arrow.height)
      .attr('marker-end', this.requestArrowMarker.bind(this))
      .attr('marker-start', this.requestArrowMarker.bind(this)));
  }

  updateBuildingDynamicFields<S extends Selection<any, any, any, any>>(selection: S): S {
    return this.updateDynamicFields(selection
      .attr('x', building => building.x)
      .attr('y', building => building.y)
      .attr('width', building => building.width)
      .attr('height', building => building.height)
      .attr('fill', building => building.fill));
  }

  updateTransform<S extends Selection<any, any, any, any>>(selection: S): S {
    return selection
      .attr('transform', building => 'scale(1) rotate(' + (building.rotate * (180 / Math.PI)) + ')');
  }

  requestArrowMarker(arrow: Arrow): string {
    let id: string;
    if (this.selectedConstruct === arrow) {
      id = 'arrow-selected';
    } else {
      id = 'arrow';
    }
    return 'url(#' + id + ')';
  }

  requestConstructStroke(building: Construct): string {
    if (this.selectedConstruct === building) {
      return LageplanComponent.selectionStroke;
    } else {
      return building.stroke;
    }
  }

  selectConstruct(construct: Construct): void {
    this.pressed = true;
    const self = this;
    this.selectedConstruct = construct;
    this.svg.selectAll('g').selectAll('rect')
      .transition().duration(350)
      .style('stroke', b => {
        if (b instanceof Construct) {
          return self.requestConstructStroke(b);
        }
        return null;
      });
    this.svg.selectAll('line')
      .attr('marker-end', this.requestArrowMarker.bind(this))
      .attr('marker-start', this.requestArrowMarker.bind(this))
      .style('stroke', b => {
        if (b instanceof Construct) {
          return self.requestConstructStroke(b);
        }
        return null;
      });
    // prevent handleMouseDown() - trigger
    this.d3.event.stopPropagation();
    // cache mouse position for moveBuilding(...)
    const coords = this.d3.mouse(this.svg.node());
    this.moveX = coords[0];
    this.moveY = coords[1];
  }

  handleMouseUpOnConstruct(building: Construct): void {
    this.pressed = false;
    // prevent handleMouseUp() - trigger
    this.d3.event.stopPropagation();
  }

  addNewBuilding(): void {
    const newBuilding = new Building();
    newBuilding.x = this.width / 2 - LageplanComponent.defaultSideLength / 2;
    newBuilding.y = this.height / 2 - LageplanComponent.defaultSideLength / 2;
    newBuilding.width = LageplanComponent.defaultSideLength;
    newBuilding.height = LageplanComponent.defaultSideLength;
    this.buildings.push(newBuilding);

    this.updateStaticFields(
      this.updateBuildingDynamicFields(
        this.svg.selectAll('g')
          .data(this.buildings).enter()
          .append('g')
          .append('rect')))
      .attr('transform', building => 'scale(0) rotate(' + (building.rotate * (180 / Math.PI)) + ')');

    this.svg.selectAll('g').selectAll('rect')
      .transition().duration(500)
      .attr('transform', building => {
        if (building instanceof Construct) {
          return 'scale(1) rotate(' + (building.rotate * (180 / Math.PI)) + ')';
        }
        return null;
      });
  }

  removeSelectedConstruct(): void {
    if (this.selectedConstruct == null) {
      alert('No Construct Selected');
    } else {
      const buildingIndex = this.buildings.indexOf(this.selectedConstruct);
      if (buildingIndex > -1) {
        this.buildings.splice(buildingIndex, 1);
        this.selectedConstruct = null;
        this.svg.selectAll('g')
          .data(this.buildings, building => {
            if (building instanceof Construct) {
              return building.pos;
            }
            return null;
          })
          .exit()
          .transition().duration(500)
          .attr('transform-origin', building => {
            if (building instanceof Construct) {
              return building.centerX() + ' ' + building.centerY();
            }
            return null;
          })
          .attr('transform', building => {
            if (building instanceof Construct) {
              return 'scale(0) rotate(0)';
            }
            return null;
          })
          .remove();
      } else {
        alert('Failed To Remove Construct');
      }
    }
  }
}
