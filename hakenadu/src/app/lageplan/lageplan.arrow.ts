import {Construct} from './lageplan.construct';

export class Arrow extends Construct {

  private static instanceCount = 0;
  public static readonly code = 999;

  constructor(code = Arrow.code) {
    super(String(Arrow.instanceCount++), code);
  }
}
