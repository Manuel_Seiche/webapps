import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dyn3Component } from './dyn3.component';

describe('Dyn3Component', () => {
  let component: Dyn3Component;
  let fixture: ComponentFixture<Dyn3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dyn3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dyn3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
