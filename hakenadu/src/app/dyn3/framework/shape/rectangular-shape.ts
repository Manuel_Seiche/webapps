import { Shape } from './shape';

export class RectangularShape extends Shape {

    private _width: number;
    private _height: number;
    private _fill: string;

    constructor() {
        super();
    }

    public get width(): number {
        return this._width;
    }

    public set width(value: number) {
        this._width = value;
    }

    public get height(): number {
        return this._height;
    }

    public set height(value: number) {
        this._height = value;
    }

    public get fill(): string {
        return this._fill;
    }

    public set fill(value: string) {
        this._fill = value;
    }
}
