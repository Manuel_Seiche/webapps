import { Shape } from './shape';

export class LinearShape extends Shape {

    private _x2: number;
    private _y2: number;

    constructor() {
        super();
    }

    public get x2(): number {
        return this._x2;
    }

    public set x2(value: number) {
        this._x2 = value;
    }

    public get y2(): number {
        return this._y2;
    }

    public set y2(value: number) {
        this._y2 = value;
    }
}
