export abstract class Shape {

    private _id: string;
    private _x: number;
    private _y: number;
    private _stroke: string;

    constructor() {

    }

    public get id(): string {
        return this._id;
    }

    public set id(id: string) {
        this._id = id;
    }

    public get x(): number {
        return this._x;
    }

    public set x(x: number) {
        this._x = x;
    }

    public get y(): number {
        return this._y;
    }

    public set y(y: number) {
        this._y = y;
    }

    public get stroke(): string {
        return this._stroke;
    }

    public set stroke(stroke: string) {
        this._stroke = stroke;
    }
}
