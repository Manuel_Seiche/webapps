import { Shape } from '../shape/shape';
import { Selection } from 'd3-ng2-service';

export interface ShapeMapping<D extends Shape> {

    enter(svg: Selection<any, any, any, any>, data: Array<D>): Selection<any, D, any, any>;

    update(svg: Selection<any, any, any, any>, data: Array<D>): Selection<any, D, any, any>;

    exit(svg: Selection<any, any, any, any>, data: Array<D>): Selection<any, D, any, any>;
}
