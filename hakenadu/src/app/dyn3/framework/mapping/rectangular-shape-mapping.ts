import { ShapeMapping } from './shape-mapping';
import { Selection, Transition } from 'd3-ng2-service';
import { RectangularShape } from '../shape/rectangular-shape';

export class RectangularShapeMapping<D extends RectangularShape> implements ShapeMapping<D> {

    public enter(svg: Selection<any, any, any, any>, data: Array<D>): Selection<any, D, any, any> {
        return this.applyDynamicAttributes(
            svg.selectAll('g').selectAll('rect')
                .data(data).enter()
                .append('g').append('rect'));
    }

    public update(svg: Selection<any, any, any, any>, data: Array<D>): Selection<any, D, any, any> {
        return this.applyDynamicAttributes(
            svg.selectAll('g').selectAll('rect')
                .data(data)
                .transition().duration(500));
    }

    /**
     * TODO
     */
    public exit(svg: Selection<any, any, any, any>, data: Array<D>): Selection<any, D, any, any> {
        return null;
    }

    private applyDynamicAttributes<S extends Selection<any, D, any, any> | Transition<any, D, any, any>>(selection: S): any {
        return selection
            .attr('x', rectangularShape => rectangularShape.x)
            .attr('y', rectangularShape => rectangularShape.y)
            .attr('width', rectangularShape => rectangularShape.width)
            .attr('height', rectangularShape => rectangularShape.height)
            .attr('fill', rectangularShape => rectangularShape.fill);
    }
}
