import { Construct } from '../app.construct';
import { Selection } from 'd3-ng2-service';

export interface Mapping<C extends Construct> {

    enter(svg: Selection<any, any, any, any>, data: Array<C>): Selection<any, C, any, any>;

    update(svg: Selection<any, any, any, any>, data: Array<C>): Selection<any, C, any, any>;

    exit(svg: Selection<any, any, any, any>, data: Array<C>): Selection<any, C, any, any>;
}
