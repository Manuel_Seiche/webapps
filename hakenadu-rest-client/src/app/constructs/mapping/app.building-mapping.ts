import { Building } from '../app.building';
import { Mapping } from './app.mapping';
import { Selection } from 'd3-ng2-service';

export class BuildingMapping implements Mapping<Building> {

    enter(svg: Selection<any, any, any, any>, data: Building[]): Selection<any, Building, any, any> {
        throw new Error('Method not implemented.');
    }

    update(svg: Selection<any, any, any, any>, data: Building[]): Selection<any, Building, any, any> {
        throw new Error('Method not implemented.');
    }

    exit(svg: Selection<any, any, any, any>, data: Building[]): Selection<any, Building, any, any> {
        throw new Error('Method not implemented.');
    }
}
