export class Construct {
    public pos: string;
    public code: string;

    constructor(pos: string, code: string) {
        this.pos = pos;
        this.code = code;
    }

    public info(): string {
        return 'pos = \'' + this.pos
            + '\', code = \'' + this.code + '\'';
    }
}
