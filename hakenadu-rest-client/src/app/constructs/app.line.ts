import { Construct } from './app.construct';

export class Line extends Construct {
    public startX: number;
    public startY: number;
    public endX: number;
    public endY: number;

    constructor(pos: string, code: string, startX: number, startY: number, endX: number, endY: number) {
        super(pos, code);
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
    }

    public info(): string {
        return super.info()
            + ', startX = ' + this.startX
            + ', startY = ' + this.startY
            + ', endX = ' + this.endX
            + ', endY = ' + this.endY;
    }
}
