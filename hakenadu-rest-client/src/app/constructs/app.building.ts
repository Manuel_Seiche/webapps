import { Construct } from './app.construct';

export class Building extends Construct {
    public x: number;
    public y: number;
    public width: number;
    public height: number;

    constructor(pos: string, code: string, x: number, y: number, width: number, height: number) {
        super(pos, code);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public info(): string {
        return super.info()
            + ', x = ' + this.x
            + ', y = ' + this.y
            + ', width = ' + this.width
            + ', height = ' + this.height;
    }
}
