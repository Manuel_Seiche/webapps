import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { AppService } from './app.service';
import { Observable } from 'rxjs/Observable';
import { Construct } from './constructs/app.construct';
import { D3, D3Service, Selection } from 'd3-ng2-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private d3: D3;
  private svg: Selection<any, any, any, any>;

  public constructs: Observable<Construct>;

  private drawWidth: number;
  private drawHeight: number;

  constructor(appService: AppService, d3Service: D3Service) {
    this.constructs = appService.getConstructs();
    this.d3 = d3Service.getD3();
  }

  ngOnInit() {
    this.drawWidth = 600;
    this.drawHeight = 600;
    this.svg = this.d3.select('body').append('svg')
      .attr('width', this.drawWidth)
      .attr('height', this.drawHeight)
      .style('border', '2px solid black');
  }
}
