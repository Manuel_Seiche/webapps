import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { Construct } from './constructs/app.construct';
import { Building } from './constructs/app.building';
import { Line } from './constructs/app.line';

@Injectable()
export class AppService {
  private http: Http;

  constructor(http: Http) {
    this.http = http;
  }

  public getConstructs(): Observable<Construct> {
    return this.http.get('http://localhost:8080/restService/').pipe(map(this.mapResponseToConstruct.bind(this)));
  }

  private mapResponseToConstruct(response: Response): Array<Construct> {
    const json = response.json();
    const returnData: Array<Construct> = [];
    for (let i = 0; i < json.length; i++) {
      const construct = json[i];
      if (construct.code === 'A') {
        returnData[i] = new Building(
          construct.pos,
          construct.code,
          construct.x,
          construct.y,
          construct.width,
          construct.height);
      } else if (construct.code === 'B') {
        returnData[i] = new Line(
          construct.pos,
          construct.code,
          construct.x,
          construct.y,
          construct.x + construct.width,
          construct.y + construct.height);
      }
    }
    return returnData;
  }
}
